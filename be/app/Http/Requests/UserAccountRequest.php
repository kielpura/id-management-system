<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class UserAccountRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'bail|required|max:50',
            'middle_name' => 'bail|nullable|max:50',
            'last_name' => 'bail|required|max:50',
            'address' => 'bail|nullable|max:100',
            'gender' => 'bail|nullable|max:15',
            'birthday' => 'bail|nullable|max:50',
            'contact_number' => 'bail|nullable|max:30',
            'emergency_number' => 'bail|nullable|max:30',
            'emergency_contatct_person' => 'bail|nullable|max:50',
            'year_level' => 'bail|nullable|max:50',
            'course' => 'bail|nullable|max:50',
            'semester' => 'bail|nullable|max:50',
        ];
    }

    //Returns a json reponse with a status code of 422
    public function failedValidation(Validator $validator){
        throw new HttpResponseException(response()->json($validator->errors(), 422));
    }
}
