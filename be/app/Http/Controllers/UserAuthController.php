<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserAccountRequest;
use App\Models\UserInfo;
use App\Models\UserAccount;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserAuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login', 'store']]);
    }
    
    public function login(Request $request)
    {
            if (! $token = auth()->guard('api')->attempt(['email' => $request->email, 'password' => $request->password])) {
                return response()->json(['error' => 'Unauthorized'], 401);
            }
            return $this->respondWithToken($token);
    }

    public function store(Request $request){
        
        $data = UserAccount::create([
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'first_name' => $request->first_name,
            'middle_name' => $request->middle_name,
            'last_name' => $request->last_name,
            'gender' => $request->gender,
            'birthday' => $request->birthday,
            'address' => $request->address,
            'contact' => $request->contact,
            'emergency_number' => $request->emergency_number,
            'emergency_contact_person' => $request->emergency_contact_person,
            'year_level' => $request->year_level,
            'course' => $request->course,
            'semester' => $request->semester,
            'student_id' => $request->status,
        ]);
        return response()->json($data);
        
    }

    protected function respondWithToken($token)
    {
        $user = UserAccount::where('id', auth('api')->user()->id)->first();
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('api')->factory()->getTTL() * 60,
        ]);
    }
}
