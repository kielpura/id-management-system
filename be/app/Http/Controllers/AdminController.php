<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UserAccount;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{
    public function getStudents(){
        $data = UserAccount::where('student_id', 'Pending')->latest()->get();
        return response()->json($data);
    }

    public function getStudentsWithId(){
        $data = UserAccount::where('student_id', '!=', 'Pending')->latest()->get();
        return response()->json($data);
    }

    public function getAllStudents(){
        $data = UserAccount::latest()->get();
        return response()->json($data);
    }

    public function updateStudent(Request $request, $id){
        $data = UserAccount::where('id', $id)->first();
        $data->update(['email' => $request->email,
                        'first_name' => $request->first_name,
                        'middle_name' => $request->middle_name,
                        'last_name' => $request->last_name,
                        'gender' => $request->gender,
                        'birthday' => $request->birthday,
                        'address' => $request->address,
                        'contact' => $request->contact,
                        'emergency_number' => $request->emergency_number,
                        'emergency_contact_person' => $request->emergency_contact_person,
                        'year_level' => $request->year_level,
                        'course' => $request->course,
                        'semester' => $request->semester,
                    ]);
        return response()->json($data);
    }

    public function updateStudentIdNumber(Request $request, $id){
        $data = UserAccount::where('id', $id)->first();
        $data->update(['student_id' => $request->student_id]);
        return response()->json($data);
    }

    public function deleteStudentRecord($id){
        UserAccount::destroy($id);
        return response()->json($id);
    }

    public function registerStudent(Request $request){
        
        $data = UserAccount::create([
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'first_name' => $request->first_name,
            'middle_name' => $request->middle_name,
            'last_name' => $request->last_name,
            'gender' => $request->gender,
            'birthday' => $request->birthday,
            'address' => $request->address,
            'contact' => $request->contact,
            'emergency_number' => $request->emergency_number,
            'emergency_contact_person' => $request->emergency_contact_person,
            'year_level' => $request->year_level,
            'course' => $request->course,
            'semester' => $request->semester,
            'student_id' => $request->status,
        ]);
        return response()->json($data);
    }
}
