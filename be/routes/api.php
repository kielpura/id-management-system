<?php

use App\Http\Controllers\AdminAuthController;
use App\Http\Controllers\UserAuthController;
use App\Http\Controllers\AdminController;
use App\Models\UserAccount;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'api', 'prefix' => 'auth'], function () {

    Route::group(['prefix' => 'admin'], function (){
        Route::post('login', [AdminAuthController::class, 'login']);
        Route::post('logout', [AdminAuthController::class, 'logout']);
        Route::post('me', [AdminAuthController::class, 'me']);

        Route::get('students', [AdminController::class, 'getStudents']);
        Route::get('students_with_id', [AdminController::class, 'getStudentsWithId']);
        Route::get('all_students', [AdminController::class, 'getAllStudents']);
        Route::post('register_student', [AdminController::class, 'registerStudent']);
        Route::put('students/{id}', [AdminController::class, 'updateStudent']);
        Route::put('students_id_number/{id}', [AdminController::class, 'updateStudentIdNumber']);
        Route::delete('students/destroy/{id}', [AdminController::class, 'deleteStudentRecord']);
    });

    Route::group(['prefix' => 'user'], function (){
        Route::post('store', [UserAuthController::class, 'store']);
        Route::post('login', [UserAuthController::class, 'login']);
        Route::post('logout', [UserAuthController::class, 'logout']);
        Route::post('me', [UserAuthController::class, 'me']);
    });
});
