<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\AdminAccount;
use Illuminate\Support\Facades\Hash;

class AdminAccountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AdminAccount::create([
            'email' => 'admin@admin.com',
            'password' => Hash::make('adminadmin')
        ]);
    }
}
