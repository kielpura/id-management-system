<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\UserAccount;
use Illuminate\Support\Facades\Hash;

class UserAccountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        UserAccount::create([
            'email' => 'ezikieltulawan@gmail.com',
            'password' => Hash::make('ezikieltulawan'),
            'first_name' => 'Ezikiel',
            'middle_name' => 'Pura',
            'last_name' => "Tulawan",
            'gender' => 'Male',
            'birthday' => '10/26/1998',
            'address' => 'Tacloban city, Leyte',
            'contact' => '0932134422',
            'emergency_number' => '09324111124',
            'emergency_contact_person' => 'Anj Marie Alejan',
            'year_level' => '3rd Year',
            'course' => 'BSIT',
            'semester' => 'Second Semester',
        ]);
        
        UserAccount::create([
            'email' => 'yvansabay@gmail.com',
            'password' => Hash::make('yvansabay'),
            'first_name' => 'Yvan',
            'middle_name' => 'Caindoy',
            'last_name' => "Sabay",
            'gender' => 'Male',
            'birthday' => '2/14/1998',
            'address' => 'Tacloban city, Leyte',
            'contact' => '0928499212',
            'emergency_number' => '09378991233',
            'emergency_contact_person' => 'Genreve Fernandez',
            'year_level' => '3rd Year',
            'course' => 'BSIT',
            'semester' => 'Second Semester',
        ]);
    }
}
