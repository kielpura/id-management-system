<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_accounts', function (Blueprint $table) {
            $table->id();
            $table->string('email')->unique();
            $table->string('password');
            $table->string('first_name');
            $table->string('middle_name')->nullable();
            $table->string('last_name');
            $table->string('profile_photo')->nullable();
            $table->string('address')->nullable();
            $table->string('gender')->nullable();
            $table->string('birthday')->nullable();
            $table->string('contact')->nullable();
            $table->string('emergency_number')->nullable();
            $table->string('emergency_contact_person')->nullable();
            $table->string('student_id')->default("Pending");
            $table->string('year_level')->nullable();
            $table->string('course')->nullable();
            $table->string('semester')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_accounts');
    }
}
