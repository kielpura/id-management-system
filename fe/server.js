const express = require('express')
const serveStatic = require('serve-static')
const path = require('path')

const app = express()

app.use('/', serveStatic(path.join(__dirname, '/dist')))
app.use('/login', serveStatic(path.join(__dirname, '/dist')))
app.use('/home/dashboard', serveStatic(path.join(__dirname, '/dist')))
app.use('/home/students', serveStatic(path.join(__dirname, '/dist')))
app.use('/home/students_registration', serveStatic(path.join(__dirname, '/dist')))
app.use('/home/id_records', serveStatic(path.join(__dirname, '/dist')))

const port = process.env.PORT || 8080
app.listen(port)