import Vue from 'vue'
import App from './App.vue'
import {BootstrapVue, IconsPlugin} from 'bootstrap-vue'
import Toast from "vue-toastification";
import router from './routes'
import store from './store'

Vue.use(BootstrapVue)
Vue.use(IconsPlugin)

import './assets/css/style.css'
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import "vue-toastification/dist/index.css";
import '@fortawesome/fontawesome-free/css/all.css'
import '@fortawesome/fontawesome-free/js/all.js'

Vue.use(Toast, {
  maxToasts: 4,
  newestOnTop: true,
  position: "top-right",
  hideProgressBar: true,
  pauseOnHover: true
});

Vue.config.productionTip = false



new Vue({
  router, store,
  render: h => h(App)
}).$mount('#app')
