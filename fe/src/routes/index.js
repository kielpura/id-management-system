import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '@/components/auth/Login.vue'
import Register from '@/components/auth/Register.vue'
import Index from '@/components/admin/Index.vue'
import Dashboard from '@/components/admin/Dashboard.vue'
import Students from '@/components/admin/Students.vue'
import StudentRegistration from '@/components/admin/StudentRegistration.vue'
import IDRecords from '@/components/admin/IDManagement.vue'

Vue.use(VueRouter)

const routes = [
      {
        path: '/login', 
        name: 'login',
        component: Login,
        meta: { hasUser: true}
      },
      {
        path: '/',
        name: 'register',
        component: Register,
        meta: { hasUser: true}
      },
      {
        path: '/home',
        name: 'Home',
        component: Index,
        meta: { isAdmin: true, requiresLogin: true },
        children: [
          {
          path:'dashboard',
          name: 'dashboard',
          components:{
            dashboard: Dashboard
            },
          },
          {
          path:'students',
          name: 'students',
          components:{
            students: Students
            },
          },
          {
          path:'students_registration',
          name: 'students_registration',
          components:{
            students_registration: StudentRegistration
            },
          },
          {
          path:'id_records',
          name: 'id_records',
          components:{
            id_records: IDRecords
            },
          },
        ]
      },

    ]

    const router = new VueRouter({
      mode: 'history',
      base: process.env.BASE_URL,
      routes
    })

    router.beforeEach((to, from, next) => {
      if (to.matched.some((record) => record.meta.requiresLogin) && !localStorage.getItem('auth')){
        next({name: 'Login'})
      }
      else if (to.matched.some((record) => record.meta.hasUser) && localStorage.getItem('auth') && localStorage.getItem('isAdmin')) {
          next({ name: "Home" });
      } 
      else if (to.matched.some((record) => record.meta.hasUser) && localStorage.getItem('auth') && localStorage.getItem('isUser')) {
          next({ name: "User" });
      } 
      else if (to.matched.some((record) => record.meta.isAdmin) && localStorage.getItem('auth') && localStorage.getItem('isUser')) {
          next({ name: "User" });
      } 
      else if (to.matched.some((record) => record.meta.isUser) && localStorage.getItem('auth') && localStorage.getItem('isAdmin')) {
          next({ name: "Home" });
      } 
      else {
        next();
      }
    });
    
    export default router