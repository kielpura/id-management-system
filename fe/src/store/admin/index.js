import API from '../base/'

export default {
  namespaced: true,
  state: {
    students: {},
    update_student: {},
    students_with_id: {},
    all_students: {},
  },
  mutations: {
    PUSH_NEW_STUDENT(state, data){
      state.all_students.push(data)
    },
    SET_STUDENTS(state, data){
      state.students = data
    },
    SET_UPDATE_STUDENT(state, data){
      state.students = data
    },
    SET_ALL_STUDENTS(state, data){
      state.all_students = data
    },
    SET_STUDENTS_WITH_ID(state, data){
      state.students_with_id = data
    }, 
    DELETE_STUDENT_RECORD(state, id){
      state.all_students = state.all_students.filter(all_students => {
        return all_students.id !== id;
      });
    },
  },
  actions: {
    async getStudents({commit}, data){
        const res = await API.get('/auth/admin/students', data).then(res => {
          commit('SET_STUDENTS', res.data)
  
          return res;
        }).catch(err => {
         return err.response;
        })
  
        return res;
      },
    async getStudentsWithId({commit}, data){
        const res = await API.get('/auth/admin/students_with_id', data).then(res => {
         commit('SET_STUDENTS_WITH_ID', res.data)
  
          return res;
        }).catch(err => {
         return err.response;
        })
  
        return res;
      },
    async getAllStudents({commit}, data){
        const res = await API.get('/auth/admin/all_students', data).then(res => {
          commit('SET_ALL_STUDENTS', res.data)
  
          return res;
        }).catch(err => {
         return err.response;
        })
  
        return res;
      },
    async updateStudent({commit}, data){
      const res = await API.put(`/auth/admin/students/${data.id}`, data).then(res => {
        commit('SET_UPDATE_STUDENT', data)
        return res;
      }).catch(err => {
        return err.response;
      })

      return res;
    },
    async updateStudentId({commit}, data){
      const res = await API.put(`/auth/admin/students_id_number/${data.id}`, data).then(res => {
          commit('SET_STUDENTS', data)
        return res;
      }).catch(err => {
        return err.response;
      })

      return res;
    },
    async deleteStudentRecord({commit}, id){
      const res = await API.delete(`/auth/admin/students/destroy/${id}`).then(res => {
        commit('DELETE_STUDENT_RECORD', id)

        return res;
      }).catch(err => {
       return err.response;
      })

      return res;
    },
    async registerStudent({commit}, data){
      const res = await API.post('/auth/admin/register_student', data).then(res => {
        commit('PUSH_NEW_STUDENT', res.data)
        return res;
      }).catch(err => {
       return err.response;
      })
  
      return res;
    },
}
}