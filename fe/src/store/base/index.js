import axios from 'axios';

const API = axios.create({
  baseURL: 'https://aclc-id-registration-backend.herokuapp.com/api/'
});

export default API;