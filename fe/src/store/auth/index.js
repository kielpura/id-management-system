import API from '../base/'

export default {
  namespaced: true,
  state: {
    user: [],
    useraccount: [],
    token: localStorage.getItem('auth') || ''
  },
  mutations: {
    PUSH_NEW_USER(state, data){
      state.user.push(data)
    },
    SET_ACC(state, data){
      state.user = data
      const bearer_token = localStorage.getItem('auth') || ''
      API.defaults.headers.common['Authorization'] = `Bearer ${bearer_token}`
    },
    SET_USER_ACC(state, data){
      state.useraccount = data
      const bearer_token = localStorage.getItem('auth') || ''
      API.defaults.headers.common['Authorization'] = `Bearer ${bearer_token}`
    },
    SET_AUTH_TOKEN(state, token) {
     localStorage.setItem('auth', token)
     localStorage.setItem('isAdmin', 'true')
     state.token = token

     const bearer_token = localStorage.getItem('auth') || ''
     API.defaults.headers.common['Authorization'] = `Bearer ${bearer_token}`
    },
    SET_AUTH_USER_TOKEN(state, token) {
     localStorage.setItem('auth', token)
     localStorage.setItem('isUser', 'true')
     state.token = token

     const bearer_token = localStorage.getItem('auth') || ''
     API.defaults.headers.common['Authorization'] = `Bearer ${bearer_token}`
    },
    UNSET_USER(state){
     localStorage.removeItem('auth');
     localStorage.removeItem('isUser');
     localStorage.removeItem('isAdmin');
     state.token = ''
     state.user = ''

     API.defaults.headers.common['Authorization'] = ''
   } 
  },
  actions: {
    async loginAccount({commit}, payload){
      const res = await API.post('/auth/admin/login', payload).then(res => {
        commit('SET_ACC', res.data)
        commit('SET_AUTH_TOKEN', res.data.access_token)

        return res;
      }).catch(err => {
       return err
      })

      return res;
    },
    async loginAccountUser({commit}, payload){
      const res = await API.post('/auth/user/login', payload).then(res => {
        commit('SET_USER_ACC', res.data)
        commit('SET_AUTH_USER_TOKEN', res.data.access_token)

        return res;
      }).catch(err => {
       return err
      })

      return res;
    },
    async logoutUser({commit}){
     const res = await API.post('auth/admin/logout?token=' + localStorage.getItem('auth')).then(response => {
       commit('UNSET_USER')
       return response
     }).catch(error => {
       return error.response
     });

     return res;
   },
   async checkUser({commit}) {
    const res = await API.post('auth/admin/me?token=' + localStorage.getItem('auth')).then(res => {
      commit('SET_ACC', res.data)
      return res;
    }).catch(error => {
      return error.response;
    })

    return res;
   },
   async createAccount({commit}, data){
    const res = await API.post('/auth/user/store', data).then(res => {
      commit('PUSH_NEW_USER', res.data)
      return res;
    }).catch(err => {
     return err.response;
    })

    return res;
  },
  },
}